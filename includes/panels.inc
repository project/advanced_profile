<?php

/**
 * @file
 * Handles integration with the Panels module.
 */

// Load the exported panel pages
function advanced_profile_default_panel_pages() {
  include_once drupal_get_path('module', 'advanced_profile') . '/includes/panels.pages.inc';
  return _advanced_profile_default_panel_pages();
}

// Load the exported view panes
function advanced_profile_default_panel_views() {
  include_once drupal_get_path('module', 'advanced_profile') . '/includes/panels.views.inc';
  return _advanced_profile_default_panel_views();
}

/**
 * Implementation of hook_panels_content_types().
 */
function advanced_profile_panels_content_types() {
  $items['author_pane'] = array(
    'title' => t('Author Pane'),
    'title callback' => 'advanced_profile_title_author_pane',
    'content_types' => 'advanced_profile_author_pane_content_type',
    'single' => TRUE,
    'render callback' => 'advanced_profile_author_pane_content',
  );

  $items['profile_visits'] = array(
    'title' => t('Profile visits'),
    'title callback' => 'advanced_profile_title_profile_visits',
    'content_types' => 'advanced_profile_profile_visits_content_type',
    'single' => TRUE,
    'render callback' => 'advanced_profile_profile_visits_content',
  );

  return $items;
}

/**
 * Return "author pane" content type.
 */
function advanced_profile_author_pane_content_type() {
  return array(
    'description' => array(
      'title' => t('Author Pane'),
      'icon' => 'icon_user.png',
      'path' => panels_get_path('content_types/user'),
      'description' => t('Author related variables gathered from helper modules.'),
      'required context' => new panels_required_context(t('User'), 'user'),
      'category' => array(t('User context'), -9),
    ),
  );
}

/**
 * Output function for the 'author pane' content type.
 */
function advanced_profile_author_pane_content($conf, $panel_args, $context) {
  $account = isset($context->data) ? drupal_clone($context->data) : NULL;
  $block->module = 'author_pane';
  $block->title = "Author Pane";

  if ($account) {
    $block->content = theme('advanced_profile_author_pane', $account);
  }
  else {
    $block->content = "User information not available";
  }

  return $block;
}

function advanced_profile_title_author_pane($conf, $context) {
  return t('"@s" author pane', array('@s' => $context->identifier));
}


/**
 * Return "author pane" content type.
 */
function advanced_profile_profile_visits_content_type() {
  return array(
    'description' => array(
      'title' => t('Profile visits'),
      'icon' => 'icon_user.png',
      'path' => panels_get_path('content_types/user'),
      'description' => t('List of visitors to a profile page.'),
      'required context' => new panels_required_context(t('User'), 'user'),
      'category' => array(t('User context'), -9),
    ),
  );
}

/**
 * Output function for the 'author pane' content type.
 */
function advanced_profile_profile_visits_content($conf, $panel_args, $context) {
  $account = isset($context->data) ? drupal_clone($context->data) : NULL;
  $block->module = 'profile_visits';
  $block->title = "Profile Visits";

  if ($account) {
    $visitors = advanced_profile_tracker($account->uid);
    if ($visitors == -1) {
      $block->content = 'Statistics modules not enabled.';
    }
    else {
      $block->content = theme('advanced_profile_profile_visits', $visitors);
    }
  }
  else {
    $block->content = "User information not available";
  }

  return $block;
}

function advanced_profile_title_profile_visits($conf, $context) {
  return t('"@s" author pane', array('@s' => $context->identifier));
}
