<?php

/**
 * @file
 * Handles integration with other modules via preprocessing and API calls.
 */

// Emulate D6's Preprocessor *************************************************/

/**
 * Call preprocess functions for a theme hook.
 *
 * By calling this in a theme function prior to calling the template callback,
 * we emulate the theme registry and preprocessor hooks of D6.
 * @param $hook
 * A theme hook that need preprocessing.
 * @param $variables
 * The theme variables array. This is used to return by reference.
 * @return
 * Return by reference the $variables array
 */
function advanced_profile_call_preprocess($hook, &$variables) {
  $functions = advanced_profile_get_preprocess($hook);
  $args = array(&$variables, $hook);
  foreach ($functions as $function) {
    call_user_func_array($function, $args);
  }
}

/**
 * Get a list of preprocess functions for a given hook.
 *
 * @param $hook
 * A theme hook that need preprocessing.
 * @return
 * An array or preprocessor function.
 */
function advanced_profile_get_preprocess($hook) {
  static $registry;

  if (!isset($registry)) {
    global $theme;
    // Check the theme registry cache; if it exists, use it.
    $cache = cache_get("advprofile_registry:$theme", 'cache');
    if (isset($cache->data) && $cache->data) {
      $registry = unserialize($cache->data);
    }
    else {
      // We'll build it below.
      $registry = array();
    }
  }

  // If we don't have registry or don't have an entry, build one and cache it.
  // This will build the registry as needed so unused functions won't be polluting it.
  if (empty($registry) || !isset($registry[$hook])) {
    global $theme;
    $registry[$hook] = _advanced_profile_build_preprocess($hook);
    cache_set("advprofile_registry:$theme", 'cache', serialize($registry));
  }

  return $registry[$hook];
}

/**
 * Helper function that does the leg work of finding preprocessor functions for a given hook.
 *
 * @param $hook
 * A theme hook that need preprocessing.
 * @return
 * An array of preprocessor functions.
 */
function _advanced_profile_build_preprocess($hook) {

  $return = array();
  // Default preprocessor prefix.
  $prefixes['template'] = 'template';
  // Add all modules so they can intervene with their own preprocessors. This allows them
  // to provide preprocess functions even if they are not the owner of the current hook.
  $prefixes += module_list();

  // Some modules in d5 already have functions that look like preprocess hooks.
  unset($prefixes['search']);

  foreach ($prefixes as $prefix) {
    if (function_exists($prefix .'_preprocess')) {
      $return[] = $prefix .'_preprocess';
    }
    if (function_exists($prefix .'_preprocess_'. $hook)) {
      $return[] = $prefix .'_preprocess_'. $hook;
    }
  }
  return $return;
}

// Preprocessor loading ******************************************************/

/**
 * Load advanced profile preprocessors includes on behalf of modules.
 */
function _advanced_profile_load_preprocessors() {
  static $finished = FALSE;
  if ($finished) {
    return;
  }

  // author-pane.inc is the extension on the includes such as contact.author-pane.inc
  advanced_profile_include('author-pane.inc');
}

/**
 * Load advanced profile files on behalf of modules.
 *
 * Blatent rip of views include system.
 */
function advanced_profile_include($file) {
  static $cache;

  $includes = array();

  if (!isset($cache->data) || !$cache->data) {
    $cache = cache_get('advprofile_includes', 'cache');
  }

  if (isset($cache->data) && $cache->data) {
    $includes = unserialize($cache->data);
  }
  else {
    $advanced_profile_path = drupal_get_path('module', 'advanced_profile') . '/modules';
    foreach (module_list() as $module) {
      $module_path = drupal_get_path('module', $module);
      if (file_exists("$module_path/$module.$file")) {
        $includes[] = "./$module_path/$module.$file";
      }
      else if (file_exists("$module_path/includes/$module.$file")) {
        $includes[] = "./$module_path/includes/$module.$file";
      }
      else if (file_exists("$advanced_profile_path/$module.$file")) {
        $includes[] = "./$advanced_profile_path/$module.$file";
      }
    }
    cache_set('advprofile_includes', 'cache', serialize($includes));
  }

  if (!empty($includes)) {
    foreach ($includes as $include) {
      require_once $include;
    }
  }
}

// We need to wrap these two in the condition otherwise we'll hit a naming
// collision when advanced forum is installed on the same site.
if (!module_exists("advanced_forum")) {
  // Integration with profile module
  /**
   * Implementation of hook_preprocess_author_pane().
   */
  function profile_preprocess_author_pane(&$variables) {
    $account_id = $variables['account']->uid;
    if ($account_id != 0) {
      $variables['profile'] = profile_view_profile($variables['account']);
    }
  }

  // Integration with contact module
  /**
   * Implementation of hook_preprocess_author_pane().
   */
  function contact_preprocess_author_pane(&$variables) {
    global $user;
    $account = $variables['account'];
    $account_id = $account->uid;
    $image_path = $variables['image_path'];

    if (($account->contact) && ($account_id != $user->uid) && ($user->uid != 0)) {
      $variables['contact'] =
        l(theme('image', "$image_path/contact.png", t('Contact user'), t('Contact user'), NULL, TRUE),
          "user/$account_id/contact", array(), NULL, NULL, FALSE, TRUE);

      $variables['contact_link'] = l(t('Contact user'), "user/$account_id/contact");
    }
  }
}

