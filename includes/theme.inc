<?php

/**
 * @file
 * Holds theme and preprocess functions.
 */

function theme_advanced_profile_author_pane($account) {
  $variables['account'] = $account;
  $variables['account_id'] = $account->uid;
  $variables['image_path'] = advanced_profile_path_to_images();

  // Call the preprocess function(s) to create all the variables
  _advanced_profile_load_preprocessors();
  advanced_profile_call_preprocess('author_pane', $variables);

  // Send the variables to our template file
  return _phptemplate_callback("author_pane", $variables, array("advanced_profile_author-pane"));
}

function advanced_profile_preprocess_author_pane(&$variables) {
  // The passed in $variables['account'] refers to the user who's info is in the pane.
  $account = $variables['account'];
  $account_id = $account->uid;

  // Get a reference to the currently logged in user.
  global $user;

  // Username
  $variables['account_name'] =  theme('username', $account);
  $variables['account_id'] = $account_id;

  // Avatar
  $variables['picture'] = theme('user_picture', $account);

  // Nothing else applies to anon users, so just stop here
  if ($account_id == 0) {
    return;
  }

  // Join date / since
  $just_date = str_replace(array('H:i', 'g:ia', ' - '), '', variable_get('date_format_short', 'm/d/Y - H:i'));
  $variables['joined'] = format_date($account->created, 'custom', $just_date);

  $variables['joined_ago'] = format_interval(time() - $account->created);

  // Online status
  $variables['last_active'] = format_interval(time() - $account->access);
  if (round((time()-$account->access)/60) < 15) {
    $variables['online_icon'] = theme('image', advanced_profile_path_to_images() . '/user-online.png', t('User is online'), t('User is online'), NULL, TRUE);
    $variables['online_status'] = t('Online');
  }
  else {
    $variables['online_icon'] = theme('image',
      advanced_profile_path_to_images() . '/user-offline.png',
      t('User offline. Last seen @time ago.', array('@time' => $variables['last_active'])),
      t('User offline. Last seen @time ago.', array('@time' => $variables['last_active'])),
      NULL,
      TRUE);
    $variables['online_status'] = t('Offline');
  }
}

function theme_advanced_profile_profile_visits($visitors) {
  if (!empty($visitors)) {
    $output = theme_item_list($visitors);
  }
  else {
    $output = t("No recent visitors.");
  }

  return $output;
}
